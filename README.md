# azure-fudamentals-studies

# Azure Fundamental concepts

Public cloud (az, aws, gcp)

Private cloud (on-prem)

Hybrid cloud (pub + priv) - Este é o melhor dos cenários. 
Permite que as aplicações rodem onde for mais apropriado. 


## Cloud advantages

OBS : Os pares de características estão separados pois 
costumam gerar dúvidas. 

- High availability - Capacidade em manter um experiência 
contínua para o usuário sem qualquer delay aparente mesmo
quando algo de errado acontece. Está associado ao tempo em 
que o provider garante que o nosso serviço estará disponível. 
Este tempo é conhecido como SLA (Service Level Agreement). 
Em geral se o provedor de cloud não cumpre com o SLA firmado
no contrato, o cliente tem um reembolso em forma de créditos 
em nossa conta.

- Tolerância à falhas - Está intimamente relacionada a 
replicação de informações. Mesmo que o seviço fique 
indisponível o usuário não percebe. Ou seja existe uma 
replicação deste serviço que assume caso algo dê errado.
No caso da Azure por exemplo, cada região conta com pelo menos
3 data centers. Se meu serviço replica entre estes 3 data
centers, se por ventura um deles ficar indisponível, meu usuário
não vai sentir este problema.  Ou seja meu sistema não ficou
indisponível pois meu sistema é tolerante à falhas. 

--- 

- Escalabilidade - Está associado ao ato de alterar recursos 
para atender uma dada demanda, ou seja, meu recurso será escalado. 
Suponha que meu servidor sofreu uma reescala para atender a um
aumento de requisições. Então aumento a memmório e o disco desta
máquina e então, meu servidor é munido de escalabilidade. Eu readequei
aquele recurso à nova demanda e não retornarei com a antiga 
configuração. 


- Elasticidade - Neste caso estabelecemos regras conhecidas como 
_scale set_. Elas estabelecem que quando a carga de trabalho 
assumir um dado valor, digamos 90% de sua capacidade, eu vou 
adicionar mais máquinas a este sistema. A diferença entre 
a escalabilidade entra agora, quando essas máquinas não forem mais
requisitadas na mesma intensidade, o sistema VOLTA a assumir a 
configuração anterior. Na escalabilidade, não voltavamos a 
configuração anterior. Escalabilidade envolve o retorno a 
configuração de inicio, se a requisicões diminuirem. Em provedores
de cloud isso pode ser feito manual ou automáticamente. 

---

- Alcance global - Todos os providers ofecerem data centers em 
diferentes localidades do planeta. Empresas multinacionais podem
dividir seus recursos em diferentes regiões para acelerar os 
acessos.

- Capacidade de Latência - Há um link direto entre as regiões 
promovendo uma latência muito menor de comunicação se comparado
a internet tradicional.

---

- Recuparacão de desastres - Importante nõa confundir com tolerãncia
a falhas. São coisas diferentes. Recuperação de desastres é quando
houve um problema, por exemplo seu ambiente foi finalizado por alguma
razão externa (catastrofe natural) e você precisa subir todo o seu 
ambiente novamente em uma outra região. Tenho que ter um 
_disaster recovery plan_ e ser capaz de subir uma réplica do meu 
ambiente rapidamente em outra região. 

- Segurança - 

https://www.youtube.com/watch?v=4ub1uGKQK6U&list=PLz3hnOImntANgM1EyWSGkY4v-7dhWURWt (29:33)

## Capital expenses vs operating expenses

Despesa de Capital (CapEx) : dinheiro reservado para infraestrutura 
e este valor vai deduzindo com o tempo. **Paga na frente** para adquirir
novos servidores por exemplo. Em geral se faz 3 orçamentos, valida
os or;amentos, acerta e pagar para o fornecedor. Feito isso, recebe 
os itens (servidores), realiza todas as configuração e libera o setor
resposável usar.  

Quando uma empresa compra computadores e servidores, eles entram como 
ativos em seu patrimonio, por que um capital foi investido. Com o 
tempo, devido sua vida útil, esses ativos sofrem uma depreciação. 
Núvens privadas são obtidas neste modelo de consumo.

Despesa Operational (OpEx) : Dinheiro gasto em serviços e produtos
e sendo cobrado por seu uso e cobrado 30 dias após o uso. Considere 
por exemplo uma máquina virtual. Quando você cria a máquina o provedor
já comecá a cobrar segundo seu uso. Aqui pagamos conforme o tempo de 
uso. Serviços de cloud entram nessa categoria. O provedor de cloud 
gerencia os custos associados à vida útil de seu espáco físico. 
OpEx é uma modelo baseado em consumo. 

No modelo opex a cobrança é baseada no uso real e por essa 
razão temos uma previsão melhor de custos. 

## Describe different cloud services

### What are cloud service models ? 

Os modelos de serviços de cloud são PaaS, IaaS e SaaS. Os modelos se 
diferenciam por diferentes níveis de responsabilidade compartilhada. 

**IaaS** - *Infrastructure-as-a-Service* - Este modelo de serviço
é o mais próximo do gerenciamento de servidores físicos. O servidor
manterá o hardware atualizado no entanto, cabe a você locatário do
serviço realizar as manutenções de SO, configurações de rede e 
demais configurações. A vantegem deste modelo é o rápido deployment
de novos recursos de computação como por exemplo subir uma máquina
virtual. Os principais representantes de IaaS nas núvens são as 
máquinas virtuais e os sistemas de armazenamento. 

![""](../apfurlan/iaas.png)

**PaaS** - *Platform-as-a-Service* - O serviço de cloud gerencia a
máquina virtual e sua conexão e o locatário realiza o deploy de
suas aplicações neste ambiente gerenciado. Neste caso abstraímos
a parte do host, do acesso ao SO e vamos para outro patamar, vamos
utilizar diretamente a plataforma. Quando criamos uma VM e nela
fazemos as instalações desejadas, não temos acesso ao hardware 
não sei em qual servidor físico ela está porém é de minha 
responsabilidade qeu eu gerencie atualizações, gerencie aplicações,
realize backups e etc. Quando estamos em PaaS, pulamos esta 
etapa. Os grandes representantes deste modelo são os serviços de 
bancos de dados. Neste caso não temos acesso ao sistema operacional
que hospeda o servidor somente à aolicação. 

Azure App Services
fornece um ambiente gerenciado pela Azure onde os desenvolvedores
podem fazer o upload de suas aplicações web sem se preocupar com 
o hardware e software necessários. 

![""](../apfurlan/paas.png)


**SaaS** - *Software-as-a-Service* - 

O principal representante são as ferramentas de email como o 
Microsoft 365 onde não temos acesso ao sistema operacional, 
não configuramos a aplicação mas aqui, apenas fazer a 
configuração para o nosso ambiente. Se compramos o mesmo 
modelo de licença, teremos os mesmo botões, os mesmos acessos,
permissões somente vamos configurar. Observe que estamos 
subindo o nível no modelo de serviço e com isso diminuímos a
responsabilidade do cliente e aumentamos a responsabilidade
do provider.  Vou aprenas fazer a gerencia das minahs contas mas 
a plataforma já está pronta. Eu não em qual SO está, não sei 
em qual databese esta, isso não é mais minha responsabilidade. 

![""](../apfurlan/saas.png)

## Mondalidades de responsabildade compartilhada

![""](../apfurlan/shared_responsibility.png)



## Computação sem servidor

Semelhamente ao modelo PaaS, soluçoes serveless permitem que 
usuários provisionem aplicações muito rápido, sem a necessidade
de gerenciar a infraestrutura. Nestas soluções, o provedor de 
serviços de nuvem provisiona, escala e gerencia automaticamente 
a infraestrutura necessária para 
executar o código.

O nome servelees não significa que não existe uma infraestrurua 
rodando a aplicação, somente que essa iunfra não é visível ao 
desenvolvedor. Sendo assim desenvolvedores conseguem aumentar
seu foco no negócio e entregar mais valor ao core. 

É comum em provedores de cloud, a conta acaba se tornando 
muito alta no longo prazo quando dependemos sempre de máquinas
virtuais para hospedar nossos serviços. Foi pensando nesta 
questão que soluções sem servidores foram desenvolvidas. Nestas
soluções, o provedor de serviços de nuvem provisiona, escala e 
gerencia automaticamente a infraestrutura necessária para 
executar o código. 

O Azure functions (AF) o Aplicativos lógicos da Azure são exemplos 
de serviços sem servidor. O AF pode ser usado em diferentes 
contextos, seja para automatizar processos ou mesmo disparando 
alertas.  

# Parte 2 

## Principais serviços do Azure

### Componentes de Arquitetura do Azure 

**Regiões :** Mais de 60 regiões compondo mais de 140 países.
Isso ocorre porque cada região é composta por um ou mais
datacenters próximos. A existência de diferentes regiões
forncem mais flexibilidade e escala para reduzir a latêcia.

É importante frisar que regiões no Brasil porque pela LGPD, 
algumas informações não podem sair do terrirório nacional. 
Isso deve estar alinhado com a arquitetura proposta no 
projeto de dados.  

**Regiões Pares :** Todas as regiões possuem uma região par. 
Estas regiões são comumente usada quando implementamos um 
ambiente de desastre recovery. Onde terei um ambiente que pode
ser a réplica do meu ambiente atual para uma enventual situação
de crise. É importante saber que a réplica dos seus serviços, 
dentro da sua região garante que eles vão continuar ativos
se uma datacenter ficar, por ventura, indisponível. Para que 
isso funcione é necessário configurar um modelo de replicaçao
para uma região secundária, região par.

**Opçoes de disponibilidade :** A MS gosta de perguntar sobre SLA.
Lembrando que SLA corresponde ao termo de disponibilidade de 
serviço, onde o provider vai garantir que um serviço vai ficar
disponível por um dado tempo por exemplo, para uma VM  única a 
MS garante uma disponibildiade de 99.9%, já para uma Zona de 
disponibilidade inteira a MS garante 99.99% de disponibildade. 
Não há SLA para *disaster recovery* (DS). Isso significa que 
a réplica de seu ambiente na região par se mantém desligado 
enquanto o DS não for ativado. Ao ativar DS o serviço precisa 
subir e o tempo que leva para ele estar disponível vai depender
de cara infraestrutura.  **Não é automático**. Existe um SLA
para modelo de serviço que será colocado no ar.   

**Zonas de disponibilidade :** Zonas de disponibilidade são
datacenters que estão distribuídos por Reogiões da Azure. 
Cara região da Azure conta com pelo menos três zonas de 
disponibilidade/datacenters. Este tipo de implementação
é particularmente bom para evitar que problemas em um 
dado datacenter afete toda a sua aplicação. Ao fazer isso
eu estou garantindo que essas máquina não estarão fisicamente
no mesmo local. Isso também garante um maior disponibilidade.

![""](../apfurlan/azs.png)


### Principais Recursos

Recursos na Azure podem ser, storage, VMs, redes,
bancos de dados, funções e etc.,  estão disponíveis 
para criar soluções em nuvem. 

OLHAR - MARKET PLACE - CATEGORIAS

A partir do Market place do portal é possível também 
pesquisar por soluções que não são MCSFT. 


Nenhum um recurso pode ser criado sem ter um Grupo de Recursos(GR). 
É possível criá-lo no momento da criação do recurso. Não estão 
associados necessariamente a uma região. É possível que um GR tenha 
serviços em diferentes regiões. Ele serve como ferramenta de 
organização de sua infraestrutura.

Contas de armazenamento é um servico bastante importante da 
azure. Nele temos 4 itens, BLOB, Files, Table ou Filas. Este
serviço atende a inúmeras necessidades desde amazenamento de 
bancos de addso, imagens de máquinas e mais. Este serviço conta
com services de replicaçào. Ele mostrará quanto o meu sistema
ficará ativo em caso de falhas.

**Grupo de Recursos :** Nenhum item é criado sem que haja um 
grupo de recursos. Recursos não restringe os recursos a 
uma região. É possível que eles estejam em diferentes
regiões. O fato de um grupo de recursos estar em região,
não quer dizer que as coisas que estao ali dentro precisam
estar nessa região também. A restrição é que não pode haver
um GR dentro de outro GR.  Um recurso nõa pode estar em 
mais de um grupo de recurso ao mesmo tempo. 

**Azure Resource Manager :** Fornece uma camada de gerenciamento 
que permite que você crie, atualize e exclua recursos na Azure. 
Toda interação do usuário com a Azure é feita por meio do ARM 
seja por meio do Portal, do Power Shell, CLI do Azure dentre 
outros. Quando solicitamos a Azure a criação de um recurso por
meio do Portal, o azure ARM está por trás dos panos fazendo
a autenticação da requisição. 

![""](../apfurlan/arm.png)

**Assinaturas do Azure :** Uma assinatura fornece acesso
autenticado e autorizado às contas da Azure. Uma vez que 
temos uma conta na Azure já podemos criar assinaturas. É
termos mais de uma assinatura. Isso porque por ela é mais
fácil de gerenciarmos os acessos e cobrança. É comum que 
assinaturas denotem Setores, Regiões e/ou Ambientes. Observe
que se a assinatura denota uma região diferente, fica 
fácil de gerenciar a cobrança. 

As assinaturas facilitam um melhor gerenciamento dos custos
e dos acessos dos colaboradores. Pdemos também ter diversas
assinaturas em uma dada conta da Azure. Outra forma de 
facilitar o gerenciamento dos custos é adicionando tags como
veremos mais a frente. 

**Grupos de Gerenciamento :** Os grupo de gerenciamento servem 
para garantir uma melhor administrações das permissões. Podem 
incluir várias assinaturas e elas irão herdar as condições 
aplicadas. Os GG podem oferecer suporte até 6 níveis de 
profundidade.

![""](../apfurlan/gg.png)

Grupos de gerenciamento estão associados ao permissionamento dos
recursos dentro de cada assinatura.


# Module 2 -  Principais Recursos do Azure

### **Serviços de computação do Azure**

São serviços sob demanda que fornecem recursos de computação
como por exemplo discos, processadores, memórias, rede e 
sistemas operacionais. São exemplos de serviços de computação
Máquinas vituais, Aplicativos (App services), Instâncias de 
container, Serviços de Kubernetes (AKS) e Area de tabalho do 
Azure.


### **Máquinas Virtuais :** É um serviço de IaaS da Azure que 
Emula computadores físicos e podem provisionar tanto máquinas 
Windows como Máquinas Linux. As podem ser porvisionadas em um 
modelo de família ou seja famílias que são destinadas a performance 
de memória, processamento, bancos de dados, dentre outros. O usuário 
deve escolher qual é a família mais se enquadra em suas necessidades. 
Observe que este serviço consiste de um IaaS da Azure.


(Imagem IU da criação da VM - (34:00) - https://www.youtube.com/watch?
v=RoQTtUi4OjE&list=PLz3hnOImntANgM1EyWSGkY4v-7dhWURWt&index=2 

Excluir com VM - Lista todos os recursos associados e exlui ou não
juntos com ela. Isso elminia problemas de Discos Orfãos


Tags - Adicionar tags em todos os recursos para facilitar o gerenciamento
do billing)

### **Serviços de Aplicativos do Azure :** Consiste de um serviço tipo 
PaaS  totalmente gerenciado pela Azure para implantar e escalar 
aplicativos Web e APIs com rapidez. O App Services suporta .NET,
.NET Core, Node, Java, Python e php. 

(Imagem IU criação do App service - (40:52) - https://www.youtube.com/watch?
v=RoQTtUi4OjE&list=PLz3hnOImntANgM1EyWSGkY4v-7dhWURWt&index=2

Isolado - terei recursos de computacã́o isolado sá para mim. 

)

- **Serviços de Contaier do Azure :** Consiste também de 
um serviço PaaS capaz de provisionar um ambiente virtualizado 
e escalável totalemente gerenciado pelo Azure. Dentros destes 
serviços existem dois tipos : 

- **Intâncias de Container do Azure :** Executa contêiner no 
 Azure sem a necessidade de configurar/gerenciar a máquina virtual.

- **Serviço de Kubernetes do Azure :** Serviço de kubernetes
 gerenciado para Azure.

-  **Área de Trabalho Virtual do Azure (AVD):** Virtualização de desktop que roda na núvem.  Modelo de terminal services onde 
 usuários podem acessar. Neste caso a máquina hospedeira é mais 
 robusta e todos acessam a mesma máquina. Podemos também ter um 
 modelo de área de trabalho virtual onde todos os recursos são 
 individuais, ou seja, cada usuário tem sua própria máquina.
 Este modelo é mais caro e menos utilizado. Este tipo de solução
 pode ser usada em casos onde um empresa não envia um notebook 
 para o funcionário, mas sim disponibiliza uma ADV para que o
funcionáqio possa acessar de sua própria máquina pessoal.  


## **Serviços de Rede do Azure**

 - **Rede virtual do Azure (VNET) :** Permite a conexão de 
 recursos do Azure tanto com a Internet como com redes
 locais. O conexão entre duas VNETs se dá por meio do processo
 de peering que será visto mais a frente.


 - **Gateway de rede virtual privada (VPN) :** usado para 
 enviar tráfego criptografado entre uma rede virtual do 
 Azure e um local na Internet pública. VPN point-to-site
 faço a instalcão na de uma aplicação na minha máquina e 
 faço a conexão com o datacenter.


## **Serviços de Armazenamento**

Dentre os serviços de armazenamento do Azure temos o serviço
tipo blob, arquivos, tabelas e filas. Eles se caracterizam
pode um modelo diferente de utilização. 

 - **Armazenamento de container (blob) :** é otimizado
 para armazenar grandes quantidades de dados  

 - **Armazentmanto em disco :** Fornece discos para máquinas
 virtuais. 

 - **Arquivos do Azure :** ???


### **Camadas de Armazenamento do Azure**

(UI image - 50:32)

Contas de armazenamento são recipiente vazios. Nele que
vamos criar blobs, files, tables ou filas. Ao iniciar o 
processo de criação da conta de aramazamento precisaremos
escolher entre o modelo standard e o modelo premium. 
Basicamente estes dois modelos serão diferenciados pela
performance e obviamente preço. No modelo premium eu 
pago pelo espaço solicitado mesmo que não esteja sendo
usado. Por exemplo se eu solicitei 5G mas só tenho 1G 
de arquivos, pagarei pelos 5G igualmente. Já no 
standard a forma de pagamente é conforme o uso pay-as-you-go

Outra diferença vai aparecer no modelo de replicação. 
No modelo Standard temos LRS,GRS,ZRS,GZRS. O L,G,ZRS
se distinguem pela performance por exemplo o LRS fará 
3 cópias dentro de um mesmo datacenter da microsoft. 
Atente para o fato que se o Datacenter tiver um problema
voce perde sua informação. Já o ZRS faz 3 cópias uma 
em cada datacenter diferente. Aqui nossa garantia se dá
no nível da Zone de disponibilidade. Se a ZD toda cair
então perderemos nosso dados. O GRS faz as 3 cópias
e ainda faz mais cópias para fora da sua reigião ou seja,
temos um replicação de região. Ainda que a região toda 
caia, o usuário terá acesso ao dado na região secundária.
Já o ZGRS é uma combina do Z e do GRS. 

Teremos também que definir as camadas de acesso. Elas 
podem ser três Quente, Frio ou Archive. 

Dados quentes são aqueles que são acessado 
frequentemente/diariamente. Neste modelo a Azure vai fazer
uma dada cobrança pelo armazenamento porém combrará um valor 
menor pelo acesso ao dado uma vez que você está fazendo-o  
com muita frequência. Para o modelo de armazenamento frio
o valor cabrado pelo armazenamento será menor que o valor 
para os dados hot no entanto, o valor pelo acesso será um 
pouco superior ao modelo quente. Já o modelo de armazenamento
Archive é um modelo de armazenamento a frio os dados são 
acessados raramente e deve ser armazenados por pelo menos 180 
dias. Neste modelo existe um SLA para fazer o restore desses 
dados por que precisamos rehidratar esses dados. É comum neste 
modelo dizer que precisamos rehidratar os dados. Esse termos 
deve ao fato de que quando salvamos informações neste repo, 
ele vai quebrar seu arquivo em diferentes partes e distribuir-los 
onde houver espaço. Por isso para fazer o restore, precisamos de 
um dado SLA. Este modelo de armazenamento é geralmente usado 
para backup de informações que não serão acessadas tão logo.

As camadas de acesso quente e fria pode ser alternadas a 
qualquer momento. De fato em ambos os modelos podemos acessar
o dado em tempo real o ponto aqui é o custo pode cada acesso. 
Já na camada archive nõa temos essa liberdade devido ao processo 
de hidratação nos dados. É possível criar políticas de transferência
de dados entre as camadas por exemplo, se um dado ficar mais de 
180 sem ser acessado ele é automáticamente movido para a camada
Archive. É possível ainda criar uma política para excluir o dado.


## **Serviços de Bancos de dados do Azure**

Serviços de bancos de dados correspondem a PaaS. Correspondem
a serviços de bancos de dados gerenciados pela Azure.

**Cosmos Database :** é um serviço de DB distribuído e 
elástico  

**Instância gerenciado de SQL do Azure :** quando estamos 
fazendo uma migração para Azure, o modelos gerenciados 
permitirão que você migres seu SQL Server "_as-is_". É 
importanto salientar aqui que, apesar de ser possível a 
migraçao de um banco local para Azure usando PaaS, é 
recomendado que, se você necessita de 100% de compatibilidade, 
utilizar o modelo IaaS para essa migração. 

Lembrando mais uma vez que em PaaS a Azure se responsabiliza
pela aplicação de patch e backups automzatizada bem como
a alta disponibilidade. 

A Azure também permite que você migre as licenças usadas em 
seu banco on-prem diminuido o custo do serviço. 




# Module 3 - Principais soluções
 
## **IoT**

é a capacidade de dispositivos de reunir, retransmitir
informações para  análise de dados. Um bom exemplo de dispositivos
IoT são dispositivos de GPS, Ambientais, de Luz,
de fumaça ou mesmo dispositivos domésticos inteligentes.
Gerenciar todas essas informções pode ser bastante difícil
e a Azure conta com serviços que podems nos auxiliar. 


**Azure IoT Central :** É uma console centralizada que nos
ajuda no gerenciamento de dispositivos IoT. É uma solucão SaaS que 
facilita conectar, monitorar e gerenciar ativos de IoT.
Consiste de uma console centralizada onde constam
todos os serviços. Facilita a conexão de novos dispositivos
e com isso, vamos fazer uma nova observação. Você poderá
acompanhar o despenho de seus dispositivos de maneiro 
consolidada e programar alertas que vão enviar notificacões
quando um deles precisar de manutenção.


**Hub IoT do Azure :** Serviço gerenciado que funciona com
um hub central de mensagens bidirecionais entre aplicativos
IoT e os dispositivos que ele gerencia. Também faz a gestão
das informações que são enviadas/recebidas.

**Azure Sphere**